<?php

namespace silverorange\DevTest\Controller;

use silverorange\DevTest\Context;
use silverorange\DevTest\Template;
use silverorange\DevTest\Model;

class ImportPosts extends Controller
{
    public function getContext(): Context
    {
        $context = new Context();
        $context->title = 'Import Posts';

        return $context;
    }

    public function getTemplate(): Template\Template
    {
        $post = new Model\Post($this->getDatabase());
        $post->importPosts();

        return new Template\ImportPosts();
    }

    protected function loadData(): void
    {
        // TODO: Load posts from database here.
        $this->posts = [];
    }
}
