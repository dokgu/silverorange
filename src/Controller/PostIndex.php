<?php

namespace silverorange\DevTest\Controller;

use silverorange\DevTest\Context;
use silverorange\DevTest\Template;
use silverorange\DevTest\Model;

class PostIndex extends Controller
{
    private array $posts = [];

    public function getContext(): Context
    {
        $context = new Context();
        $context->title = 'Posts';
        $context->data['posts'] = $this->posts;

        return $context;
    }

    public function getTemplate(): Template\Template
    {
        $this->loadData();
        return new Template\PostIndex();
    }

    protected function loadData(): void
    {
        $post = new Model\Post($this->getDatabase());
        $this->posts = $post->getPosts();
    }
}
