<?php

namespace silverorange\DevTest\Template;

use silverorange\DevTest\Context;

class ImportPosts extends Layout
{
    protected function renderPage(Context $context): string
    {
    	// @codingStandardsIgnoreStart
        return <<<HTML
<h1>IMPORT POSTS</h1>
<p>Please note, re-importing the posts would throw an error because this doesn't check for duplicate keys.</p>
<p>To try re-importing the posts:</p>
<pre style="text-align: left; white-space: pre-wrap;">
# Reset the database
docker compose down --volumes
docker compose up --detach

# Import the tables/fixtures
docker compose exec -- db psql silverorange silverorange < sql/authors.sql
docker compose exec -- db psql silverorange silverorange < sql/posts.sql
</pre>
HTML;
        // @codingStandardsIgnoreEnd
    }
}
