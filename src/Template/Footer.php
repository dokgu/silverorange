<?php

namespace silverorange\DevTest\Template;

use silverorange\DevTest\Context;

/**
 * Template for common header content
 */
class Footer implements Template
{
    public function render(Context $context): string
    {
        $year = date('Y');

        // @codingStandardsIgnoreStart
        return <<<HTML
            </div>
        </main>
        <footer class="footer">
            <small class="copyright">© {$year} silverorange, Inc. — All rights reserved.</small>
        </footer>
        <script type="text/javascript" src="/assets/libraries/jquery/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="/assets/libraries/showdown/dist/showdown.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                if( $('.markdown').length ) {
                    let converter = new showdown.Converter();
                    $('.markdown').each( function(index) {
                        let md = $(this).text();
                        $(this).html( converter.makeHtml(md) );
                    } );
                }
            });
        </script>
    </body>
</html>
HTML;
        // @codingStandardsIgnoreEnd
    }
}
