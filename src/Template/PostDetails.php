<?php

namespace silverorange\DevTest\Template;

use Datetime;
use silverorange\DevTest\Context;

class PostDetails extends Layout
{
    protected function renderPage(Context $context): string
    {
        $html = [];

        $post = $context->data['post'];

        $created_at = DateTime::createFromFormat('Y-m-d H:i:s', $post->created_at);
        $modified_at = DateTime::createFromFormat('Y-m-d H:i:s', $post->modified_at);

        // @codingStandardsIgnoreStart
        $html[] = <<<HTML
<div id="post-{$post->id}" class="post-wrapper">
    <div class="post-title-wrapper">
        <div class="post-icon">
            <i class="fa fa-book fa-fw"></i>
        </div>
        <div class="post-details">
            <h1 class="post-title">{$post->title}</h1>
            <div class="post-details-wrapper">
                <p class="detail">
                    <i class="fa fa-user-circle fa-fw"></i>
                    <span class="value">{$post->author}</span>
                </p>
                <p class="detail">
                    <i class="fa fa-pencil fa-fw"></i>
                    <span class="value">{$created_at->format('M j, Y h:ia')} (modified {$modified_at->format('M j, Y h:ia')})</span>
                </p>
            </div>
        </div>
    </div>
    <div class="post-body markdown">{$post->body}</div>
</div>
HTML;
        // @codingStandardsIgnoreEnd

        return implode("\n", $html);
    }
}
