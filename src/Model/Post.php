<?php

namespace silverorange\DevTest\Model;

class Post
{
    protected \PDO $db;

    public string $id;
    public string $title;
    public string $body;
    public string $created_at;
    public string $modified_at;
    public string $author;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Imports all available data (/data/*.json) into the database.
     *
     * @return void
     */
    public function importPosts(): void
    {
        $dir = dirname(dirname(dirname(__FILE__))) . '/data';
        $posts = scandir($dir);
        $posts = array_slice($posts, 2);

        foreach ($posts as $post) {
            $json_data = file_get_contents($dir . '/' . $post);
            $data = json_decode($json_data, true);

            $this->insertPost(
                $data['id'],
                $data['title'],
                $data['body'],
                $data['created_at'],
                $data['modified_at'],
                $data['author']
            );
        }
    }

    /**
     * Insert a single post to the database.
     *
     * @param string $id          The UUID of the post
     * @param string $title       The title of the post
     * @param string $body        The body of the post (Markdown)
     * @param string $created_at  Date the post was created
     * @param string $modified_at Date the post was modified
     * @param string $author      The UUID of the post author
     * @return void
     */
    public function insertPost($id, $title, $body, $created_at, $modified_at, $author): void
    {
        // Prepare statement for insert
        $sql = "INSERT INTO Posts (id, title, body, created_at, modified_at, author)
            VALUES (:id, :title, :body, :created_at, :modified_at, :author)";
        $stmt = $this->db->prepare($sql);

        // Bind values to the statement
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':body', $body);
        $stmt->bindValue(':created_at', $created_at);
        $stmt->bindValue(':modified_at', $modified_at);
        $stmt->bindValue(':author', $author);

        // Execute the insert statement
        $stmt->execute();
    }

    /**
     * Get all available posts from the database.
     *
     * @return array
     */
    public function getPosts(): array
    {
        // Prepare statement for getting all the posts
        $sql = "SELECT
                p.id,
                p.title,
                p.body,
                p.created_at,
                p.modified_at,
                a.full_name AS author
            FROM Posts p
                LEFT JOIN Authors a ON a.id = p.author
            ORDER BY
                p.created_at DESC";
        $stmt = $this->db->query($sql);

        $posts = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $posts[] = [
                'id' => $row['id'],
                'title' => $row['title'],
                'body' => $row['body'],
                'created_at' => $row['created_at'],
                'modified_at' => $row['modified_at'],
                'author' => $row['author']
            ];
        }

        return $posts;
    }

    /**
     * Set the post data from the database.
     *
     * @param string $id The UUID of the post
     */
    public function getPost($id): void
    {
        // Prepare statement for getting all the posts
        $sql = "SELECT
                p.id,
                p.title,
                p.body,
                p.created_at,
                p.modified_at,
                a.full_name AS author
            FROM Posts p
                LEFT JOIN Authors a ON a.id = p.author
            WHERE p.id = :id";
        $stmt = $this->db->prepare($sql);

        $stmt->execute(['id' => $id]);

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        $this->id = $row['id'];
        $this->title = $row['title'];
        $this->body = $row['body'];
        $this->created_at = $row['created_at'];
        $this->modified_at = $row['modified_at'];
        $this->author = $row['author'];
    }
}
